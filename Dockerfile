FROM alpine:latest

LABEL maintainer="Autotoolr <https://gitlab.com/autotoolr-docker>" cli_version="1.22.56"

RUN apk --update add python3 bash && \
    if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
    apk add --virtual=build gcc libffi-dev musl-dev openssl-dev python3-dev make && \    
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools wheel && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    pip3 install --upgrade awscli==1.22.56 && \
    apk --purge del build && \
    rm /var/cache/apk/*